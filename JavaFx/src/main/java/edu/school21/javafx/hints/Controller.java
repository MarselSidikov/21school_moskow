package edu.school21.javafx.hints;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller implements Initializable {

    @FXML
    private Button connectionButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        connectionButton.setOnAction(event -> {
            // создаем socket-client
            // client = new SocketClient();
            // клиент подключается к серверу по нужному ip и нужному порту
            // client.startConnection("127.0.0.1", 7777);
            // создаем слушателя сервера
            // ReceiveMessageTask task = new ReceiveMessageTask(client.getIn(), this);
            // в побочном потоке запускаем слушатель
            ExecutorService service = Executors.newFixedThreadPool(1);
            // service.execute(task);
            connectionButton.getScene().getRoot().requestFocus();
            connectionButton.setText("Подключено");
            connectionButton.setDisable(true);
        });
    }
}