package edu.school21.javafx.hints;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;

// класс отвечает за чтение сообщений от сервера в JavaFX потоке
public class ReceiveMessageTask extends Task<Void> {
    // читаем сообщения от сервера
    private BufferedReader in;
    // основной контроллер приложения
    private Controller controller;

    public ReceiveMessageTask(BufferedReader in, Controller controller) {
        this.in = in;
        this.controller = controller;
    }

    // этот метод выполняется в отдельном javaFX-потоке
    @Override
    public Void call() {
        // пока игра работает (пока игру не остановили)
        while (true) {
            String serverMessage = null;
            try {
                // считываем сообщение с сервера
                serverMessage = in.readLine();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            // если сообщение не пустое
            if (serverMessage != null) {
                // если с сервера пришло сообщение "SHOT"
                if (serverMessage.startsWith("SHOT")) {
                    // запускаем в еще одном побочном потоке, но уже в UI
                    Platform.runLater(() -> {
                        // загружаю изображение с пулей пулю
                        ImageView bullet = new ImageView("/images/enemyBullet.png");
                        // задааю начальные координаты пули
                        bullet.setX(10);
                        bullet.setY(10);
                        // запускаю анимацию
                        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
                            if (bullet.isVisible()) {
                                // я именю у кнопки Y
                                bullet.setY(bullet.getY() + 1);
                            }
                        }));
                        // счетчик цикла
                        timeline.setCycleCount(500);
                        // запускаем анимацию
                        timeline.play();
                    });
                }
            }
        }
    }
}
