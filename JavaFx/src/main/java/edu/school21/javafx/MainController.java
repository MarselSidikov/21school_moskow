package edu.school21.javafx;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 25.10.2020
 * JavaFx
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainController implements Initializable {
    @FXML
    private Button helloButton;

    @FXML
    private Label helloLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        helloButton.setOnMouseClicked(value -> {
            helloLabel.setText("Hello!");
        });
    }
}
