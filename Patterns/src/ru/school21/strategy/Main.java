package ru.school21.strategy;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        SortAlgorithm bubbleSort = sequence -> {
            for (int i = sequence.length - 1; i >= 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (sequence[j] > sequence[j + 1]) {
                        int temp = sequence[j];
                        sequence[j] = sequence[j + 1];
                        sequence[j + 1] = temp;
                    }
                }
            }
        };

        SortAlgorithm selectionSort = sequence -> {
            for (int i = 0; i < sequence.length; i++) {
                int min = sequence[i];
                int indexOfMin = i;
                for (int j = i; j < sequence.length; j++) {
                    if (sequence[j] < min) {
                        min = sequence[j];
                        indexOfMin = j;
                    }
                }
                sequence[indexOfMin] = sequence[i];
                sequence[i] = min;
            }
        };

        SearchUtilWithSortImpl searchUtil = new SearchUtilWithSortImpl();

        searchUtil.setSequence(new int[]{14, -10, -20, 100, 25, -7});

        searchUtil.setSortAlgorithm(selectionSort);

        System.out.println(searchUtil.search(-20));
        System.out.println(searchUtil.search(100));
        System.out.println(searchUtil.search(25));
        System.out.println(searchUtil.search(50));
        System.out.println(searchUtil.search(-21));
        System.out.println(searchUtil.search(101));
    }
}
