package ru.school21.strategy;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchUtilWithSortImpl implements SearchUtil, SortContext {

    private int sequence[];

    private SortAlgorithm sortAlgorithm;

    @Override
    public boolean search(int value) {
        sort();

        return binarySearch(this.sequence, 0, this.sequence.length, value) != -1;
    }

    private static int binarySearch(int array[], int left, int right, int numberForSearch) {
        if (left == right) {
            return -1;
        }

        int middle = left + (right - left) / 2;

        if (array[middle] < numberForSearch) {
            return binarySearch(array,middle + 1,right, numberForSearch);
        } else if (array[middle] > numberForSearch) {
            return binarySearch(array, left, middle, numberForSearch);
        } else {
            return middle;
        }
    }

    @Override
    public void setSequence(int[] sequence) {
        this.sequence = sequence;
    }

    @Override
    public void sort() {
        this.sortAlgorithm.sort(this.sequence);
    }

    @Override
    public void setSortAlgorithm(SortAlgorithm algorithm) {
        this.sortAlgorithm = algorithm;
    }
}
