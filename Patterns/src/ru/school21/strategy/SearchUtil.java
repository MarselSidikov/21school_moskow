package ru.school21.strategy;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SearchUtil {
    boolean search(int value);
    void setSequence(int sequence[]);
}
