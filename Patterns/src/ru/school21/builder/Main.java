package ru.school21.builder;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        User user = new User(1,
//                "Марсель",
//                "Сидиков",
//                26,
//                true,
//                false);
//
//        User user1 = new User(null, "Пользователь",
//                "Пользователев",
//                null,
//                null,
//                null);
//
//        User user2 = new User("Пользователь", "Пользователев");

        User user = User.builder()
                .id(1)
                .age(26)
                .lastName("Сидиков")
                .firstName("Марсель")
                .isWorker(true)
                .isStudent(false)
                .build();

        User user1 = User.builder()
                .firstName("Пользователь")
                .lastName("Пользователев")
                .build();
    }
}
