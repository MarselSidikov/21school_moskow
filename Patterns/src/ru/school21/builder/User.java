package ru.school21.builder;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Boolean isWorker;
    private Boolean isStudent;

    private User(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName =builder.lastName;
        this.age = builder.age;
        this.isWorker =builder.isWorker;
        this.isStudent = builder.isStudent;
    }

    public static class Builder {
        private Integer id;
        private String firstName;
        private String lastName;
        private Integer age;
        private Boolean isWorker;
        private Boolean isStudent;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(Integer age) {
            this.age = age;
            return this;
        }

        public Builder isWorker(Boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public Builder isStudent(Boolean isStudent) {
            this.isStudent = isStudent;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public Boolean getWorker() {
        return isWorker;
    }

    public Boolean getStudent() {
        return isStudent;
    }
}
