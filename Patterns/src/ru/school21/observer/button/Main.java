package ru.school21.observer.button;


/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Button button = new Button();

//        ButtonHandler handler = new ButtonHandler() {
//            @Override
//            public void handle() {
//                System.err.println("Кнопку нажали");
//            }
//        };

//        ButtonHandler handler = () -> System.err.println("Кнопку нажали");

        button.onClick(() -> System.err.println("Кнопку нажали"));

        button.click();
    }
}
