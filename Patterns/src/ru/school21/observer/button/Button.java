package ru.school21.observer.button;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Button {
    private ButtonHandler handler;

    public void onClick(ButtonHandler handler) {
        this.handler = handler;
    }

    public void click() {
        handler.handle();
    }
}
