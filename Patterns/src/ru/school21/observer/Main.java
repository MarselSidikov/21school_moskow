package ru.school21.observer;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        TextProcessor textProcessor = new TextProcessor();
        CharacterObserver observer1 = new DigitsObserverImpl();
        CharacterObserver observer2 = new LettersObserverImpl();
        CharacterObserver observer3 = new UpperCaseObserverImpl();

        textProcessor.addObserver(observer1);
        textProcessor.addObserver(observer2);
        textProcessor.addObserver(observer3);

        textProcessor.process("Hello, 123");
    }
}
