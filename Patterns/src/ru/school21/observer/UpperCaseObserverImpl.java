package ru.school21.observer;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UpperCaseObserverImpl implements CharacterObserver {
    @Override
    public void handle(char c) {
        if (Character.isUpperCase(c)) {
            System.out.println("Еще и большая!");
        }
    }
}
