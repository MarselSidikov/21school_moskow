package ru.school21.observer;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DigitsObserverImpl implements CharacterObserver {
    @Override
    public void handle(char c) {
        if (Character.isDigit(c)) {
            System.out.println("ЦИФРА - " + c);
        }
    }
}
