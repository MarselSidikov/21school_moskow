package ru.school21.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TextProcessor implements TextProcessorObservable {

    private List<CharacterObserver> observers;

    public TextProcessor() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void addObserver(CharacterObserver observer) {
        this.observers.add(observer);
    }

    public void process(String text) {
        char characters[] = text.toCharArray();
        for (char c : characters) {
            notifyObservers(c);
        }
    }

    @Override
    public void notifyObservers(char c) {
        for (CharacterObserver observer : observers) {
            observer.handle(c);
        }
    }
}
