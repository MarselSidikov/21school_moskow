package ru.school21.observer;

/**
 * 18.10.2020
 * Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CharacterObserver {
    void handle(char c);
}
