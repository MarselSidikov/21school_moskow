package ru.school21.exceptions;

import java.util.Scanner;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainNullPointerException {
    public static void main(String[] args) {
        Scanner scanner = null;
        int a = scanner.nextInt();
    }
}
