package ru.school21.exceptions;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStackoverflow {
    public static int f(int n) {
//        if (n == 0) {
//            return 1;
//        }
        return f(n - 1) * n;
    }
    public static void main(String[] args) {
        System.out.println(f(5));
    }
}
