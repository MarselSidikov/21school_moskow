package ru.school21.exceptions;

import java.io.*;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainFileNotFoundException {
    public static void main(String[] args) throws FileNotFoundException {
        InputStream input = new FileInputStream("input.txt");
    }
}
