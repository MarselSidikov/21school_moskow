package ru.school21.exceptions;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutOfMemoryError {
    public static void main(String[] args) {
        String text[] = new String[Integer.MAX_VALUE];
    }
}
