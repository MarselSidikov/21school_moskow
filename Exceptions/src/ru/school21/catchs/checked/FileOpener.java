package ru.school21.catchs.checked;

import java.io.*;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileOpener {
    public InputStream open(String fileName) {
//        try {
//            return new FileInputStream(fileName);
//        } catch (FileNotFoundException e) {
//            return null;
//        }
        try {
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int readFromInputStream(InputStream inputStream) {
        try {
            return inputStream.read();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
