package ru.school21.catchs.checked;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        FileOpener fileOpener = new FileOpener();
        InputStream inputStream = fileOpener.open("input.txt");
        System.out.println(fileOpener.readFromInputStream(inputStream));
    }
}
