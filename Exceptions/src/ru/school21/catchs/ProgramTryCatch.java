package ru.school21.catchs;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 18.10.2020
 * Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProgramTryCatch {

    public static int div(int a, int b) {
        return  a / b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        try {
//            int a = scanner.nextInt();
//            int b = scanner.nextInt();
//            int c = div(a, b);
//            System.out.println(c);
//        } catch (ArithmeticException e) {
//            System.out.println("Вы ввели неверное число!");
//        } catch (InputMismatchException e) {
//            System.out.println("Вы ввели не число, а что-то другое");
//        }

        try {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int c = div(a, b);
            System.out.println(c);
        } catch (RuntimeException e) {
            System.out.println("Вы ввели не число а что, то другое, подробнее : " + e.getMessage());
        }

    }
}
