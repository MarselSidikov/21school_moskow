`Exception` как термин - исключительная ситуация в java, которая произошла во время выполнения программы.

* Вывод информации об исключении

```
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at ru.school21.Main.div(Main.java:8)
	at ru.school21.Main.main(Main.java:16)
```
# Классы исключений

* `ArithmeticException` - класс, объекты которого описывают арифметические ошибки. Потомок - `RuntimeException`

* `NullPointerException` - класс, объекты которого описывают ошибки, связанные с нулевой ссылочной переменной. Потомок - `RuntimeException`

* `OutOfMemoryError` - класс, объекты которого описывают ошибки, связанные с переполнением памяти. Потомок - `VirtualMachineError`.

* `StackOverflowError` - класс, объекты которого описывают ошибки, связанные с переполнением стека вызовов. Потомок `VirtualMachineError`.

* `FileNotFoundException` - класс, объекты которого описывают ошибки, связанные с необнаружением файла. Потомок `IOException`.

* `RuntimeException` - потомок `Exception`.

* `VirtualMachineError` - ошибки, связанные с проблемами виртуальной машины. Потомок `Error`.

* `IOException` - ошибки, связанные с потоком ввода-вывода. потомок `Exception`.

* `Error` - потомок `Throwable`.

* `Exception` - потомок `Throwable`. 

* `Throwable` - предок всех исключительных ситуаций в java. Описывает поведение и состояние всех исключений.

```java
public class Throwable {
	// детализированное сообщение об ошибке
	private String detailMessage;

	private Throwable cause = this; // причина ситуации, она может быть другой ошибкой
	private StackTraceElement[] stackTrace; // стек вызовов, который привел к ошибке
}


public final class StackTraceElement {
	// класс
	private String declaringClass;
	// название метода
    private String methodName;
    // название файла
    private String fileName;
    // строка, где произошло исключение
    private int    lineNumber;
} 
```

## Проверяемые исключения

* Исключения, которые необходимо либо обработать "на месте", либо пробросить наверх.

* Пробросить наверх, с помощью `throws` - в принципе плохая практика, потому что вы заставляете вызывающий код обраотать ошибку, которая произошла ниже. И тут два варианта - либо ее обработают, либо снова пробросят наверх.

* Правильное решение - обработать такую ошибку сразу же, с помощью конструкции try-catch.

