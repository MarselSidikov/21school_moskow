create table account
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    age        integer not null default 1
);

insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 26);
insert into account(first_name, last_name, age)
values ('Рафис', 'Тухватуллин', 34);
insert into account(first_name, last_name, age)
values ('Руслан', 'Хабибрахманов', 33);


-- запрос на получение всех данных из таблицы
select *
from account;

-- запрос на получение имен и возраста + сортировка по убыванию возрасту
-- если возраст совпал, то по возрастанию имени
select first_name, age
from account
order by age desc, first_name;

-- получение по условию
select *
from account
where age > 30;

-- обновление записей
update account
set age = 27
where id = 1;
update account
set age = 27
where last_name = 'Сидиков';

create table car
(
    id       serial primary key,
    model    varchar(100),
    color    varchar(100),
    owner_id int,
    foreign key (owner_id) references account (id)
);

insert into car(model, color, owner_id)
VALUES ('BMW', 'black', null),
       ('Lada Calina', 'black', 2),
       ('Cherry', 'grey', 3);

alter table car
    add number varchar(20) not null default 'A001AA';

-- подзапрос на получение водителя
explain analyse
select first_name, age
from account
where id in (
    select owner_id
    from car
    where color = 'black');

-- пересечение таблиц
select *
from account a
         inner join car c on a.id = c.owner_id;

select *
from account a
         left join car c on a.id = c.owner_id;

select *
from account a
         right join car c on a.id = c.owner_id;

select *
from account a
         full outer join car c on a.id = c.owner_id;



-- drop table car;
-- drop table account;
-- delete
-- from account
-- where id = 2;

select *
from account
order by id
limit 2 offset 4;


with paginated_account as (select *
                           from account order by id
                           limit ? offset ?)
select * from paginated_account pa left join car c on pa.id = c.owner_id;

alter  table account add login varchar(20);
alter  table account add email varchar(20);
alter  table account add password varchar(20);