package models;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * 19.10.2020
 * 16. Databases
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Account {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private List<Car> cars;

    public Account(Integer id, String firstName, String lastName, Integer age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.cars = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("cars=" + cars.toString())
                .toString();
    }
}
