import models.Account;

import java.util.List;

/**
 * 19.10.2020
 * 16. Databases
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountRepository {
    Account findById(Integer id);
    List<Account> findAll(int page, int size);
}
