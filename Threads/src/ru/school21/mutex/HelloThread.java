package ru.school21.mutex;

/**
 * 18.10.2020
 * Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HelloThread extends Thread {
    private final Mutex mutex;

    public HelloThread(Mutex mutex) {
        this.mutex = mutex;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            synchronized (mutex) {
                System.out.println("Hello, ");
                System.out.println("Marsel, ");
                System.out.println("How are you?");
            }
        }
    }
}
