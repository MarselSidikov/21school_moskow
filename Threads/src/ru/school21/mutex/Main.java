package ru.school21.mutex;

public class Main {

    public static void main(String[] args) {
        Mutex mutex = new Mutex();
	    ByeThread byeThread = new ByeThread(mutex);
	    HelloThread helloThread = new HelloThread(mutex);

	    byeThread.start();
	    helloThread.start();
    }
}
