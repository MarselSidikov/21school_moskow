package ru.school21.mutex;

/**
 * 18.10.2020
 * Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ByeThread extends Thread {
    private final Mutex mutex;

    public ByeThread(Mutex mutex) {
        this.mutex = mutex;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            synchronized (mutex) {
                System.out.println("Bye!");
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
                System.out.println("You are good programmer");
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
                System.out.println("See you later");
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
