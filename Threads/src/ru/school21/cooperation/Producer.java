package ru.school21.cooperation;

public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isConsumed()) {
                    System.out.println("Producer - вызвал wait");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
                System.out.println("Producer - вышел из wait");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException();
                }
                System.out.println("Producer подготовил");
                product.produce();
                System.out.println("Producer - вызвал notify");
                product.notify();
            }
        }
    }
}

