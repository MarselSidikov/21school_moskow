package ru.school21.cooperation;

public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isProduced()) {
                    System.out.println("Consumer - wait");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException();
                    }
                }
                System.out.println("Cosumer - вышел из wait");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException();
                }
                product.consume();
                System.out.println("Consumer - вызвал notify");
                product.notify();
            }
        }
    }
}
