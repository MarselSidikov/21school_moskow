package ru.school21;

import java.util.List;

/**
 * 21.10.2020
 * 17. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainForNumbersProcessor {
    public static void main(String[] args) {
        NumbersProcessor numbersProcessor = new NumbersProcessor(new ParserImpl());
        List<Boolean> result = numbersProcessor.checkEven(new String[]{"12345", "1234", "123", "12"});
        System.out.println(result);
    }
}
